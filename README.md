# Watching Clock

A wall clock that stops running when someone looks at it, catching up with the time once there's no one looking.