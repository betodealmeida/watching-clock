from __future__ import print_function

from Queue import Queue, Empty
from threading import Thread
import time

import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
import RPi.GPIO as GPIO

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
camera.vflip = True
rawCapture = PiRGBArray(camera, size=(640, 480))


PIN_A = 2
PIN_B = 3
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_A, GPIO.OUT)
GPIO.setup(PIN_B, GPIO.OUT)
GPIO.output(PIN_A, GPIO.LOW)
GPIO.output(PIN_B, GPIO.LOW)


def tick(tick_pin, area):
    print(tick_pin)
    GPIO.output(tick_pin, GPIO.HIGH)
    time.sleep(0.1)
    GPIO.output(tick_pin, GPIO.LOW)

    # delay is smaller the closer people are
    a = 0.1
    b = 0.8
    k = 18 
    frac = min(1, float(area) * k / (640 * 480))
    x = 1 - frac
    delay = a + b * x
    print(delay)
    time.sleep(delay)


def face_recognizer(q):
    time.sleep(1)  # camera warmup
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=False):
        start = time.time()
        image = frame.array
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # detect faces
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.cv.CV_HAAR_SCALE_IMAGE,
        )
        for x, y, w, h in faces:
            cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
        cv2.imshow('Faces', image)
        cv2.waitKey(1)

        # compute proximity
        area = 0
        for x, y, w, h in faces:
            area += w * h
        q.put(area)

        rawCapture.truncate(0)

        elapsed = time.time() - start
        print('processed frame in {0} seconds'.format(elapsed))


def main():
    start = int(time.time())
    ticks = 0
    tick_pin = PIN_A

    q = Queue(maxsize=1)
    area = 0

    worker = Thread(target=face_recognizer, args=(q,))
    worker.setDaemon(True)
    worker.start()

    while True:
        try:
            area = q.get_nowait()
        except Empty:
            pass  # use last value

        elapsed = int(time.time() - start)
        if ticks < elapsed or area:
            tick(tick_pin, area)
            ticks += 1
            tick_pin = PIN_A if tick_pin == PIN_B else PIN_B


if __name__ == '__main__':
    main()
