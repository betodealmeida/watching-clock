from __future__ import print_function

from Queue import Queue, Empty
from threading import Thread
import time

import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
import RPi.GPIO as GPIO

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
smileCascade = cv2.CascadeClassifier('haarcascade_smile.xml')

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
#camera.vflip = True
rawCapture = PiRGBArray(camera, size=(640, 480))


PIN_A = 2
PIN_B = 3
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_A, GPIO.OUT)
GPIO.setup(PIN_B, GPIO.OUT)
GPIO.output(PIN_A, GPIO.LOW)
GPIO.output(PIN_B, GPIO.LOW)


def tick(tick_pin):
    print(tick_pin)
    GPIO.output(tick_pin, GPIO.HIGH)
    time.sleep(0.1)
    GPIO.output(tick_pin, GPIO.LOW)
    time.sleep(0.1)


def face_recognizer(q):
    time.sleep(1)  # camera warmup
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=False):
        start = time.time()
        image = frame.array
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # detect faces
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.cv.CV_HAAR_SCALE_IMAGE,
        )

        # detect smiles
        has_smile = False
        for x, y, w, h in faces:
            h /= 2
            y += h
            cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
            face = gray[y:y+h, x:x+w]
            face_frame = image[y:y+h, x:x+w]
            smiles = smileCascade.detectMultiScale(
                face,
                scaleFactor=1.1,
                minNeighbors=15,
                minSize=(15, 15),
                flags=cv2.cv.CV_HAAR_SCALE_IMAGE,
            )
            for x, y, w, h in smiles:
                cv2.rectangle(face_frame, (x, y), (x+w, y+h), (255, 0, 0), 1)
            has_smile = smiles != ()
            if has_smile:
                print('Found smile!')
                break

        cv2.imshow('Smiles', image)
        cv2.waitKey(1)

        q.put(has_smile)

        rawCapture.truncate(0)

        elapsed = time.time() - start
        print('processed frame in {0} seconds'.format(elapsed))


def main():
    start = int(time.time())
    ticks = 0
    tick_pin = PIN_A

    q = Queue(maxsize=1)
    has_smile = False

    worker = Thread(target=face_recognizer, args=(q,))
    worker.setDaemon(True)
    worker.start()

    while True:
        try:
            has_smile = q.get_nowait()
        except Empty:
            pass  # use last value

        elapsed = int(time.time() - start)
        if ticks < elapsed or has_smile:
            tick(tick_pin)
            ticks += 1
            tick_pin = PIN_A if tick_pin == PIN_B else PIN_B


if __name__ == '__main__':
    main()
